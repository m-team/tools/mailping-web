import React from 'react';
import { withRouter } from 'react-router-dom';

class PrivacyComponent extends React.Component {
  render() {
    return (
      <article className="markdown-body">
        <h1 id="mailping-privacy-policy-and-description">Mailping privacy policy and description</h1>
        <h2 id="description-of-the-service">Description of the service</h2>
        <p>The Service <strong>Mailping</strong> sends emails to registered administrators of services and records the
          time it takes the admin to answer that email. This is necessary to comply with the REFEDS <a
            href="https://refeds.org/sirtfi">Security Incident Response Framework (Sirtfi)</a>.</p>
        <h2 id="processed-data">Processed data</h2>
        <h3 id="the-following-scopes-are-requested-from-the-oidc-provider-server">The following scopes are requested
          from the OIDC-Provider server:</h3>
        <pre><code>
          - OpenID
          - Profile
        </code></pre>
        <h3 id="example-of-information-released-by-google-when-asked-for-the-openid-scope">Example of information
          released by Google when asked for the OpenID Scope</h3>
        <pre><code>
          - profile
          - family_name
          - Name
          - picture
          - Issuer
          - Gender
          - given_name
          - Subject
        </code></pre>
        <h2 id="purpose-for-the-processing-of-personal-data">Purpose for the processing of personal data</h2>
        <p>To provide the <strong>Mailping</strong> service, personal data and log files are collected and used for:
        </p>
        <pre><code>
          - User authentication and authorization at the service
          - Automated sending of email messages necessary for the use of the service
          - Statistics and development of the service
        </code></pre>
        <h2 id="regular-disclosure-of-personal-data-to-third-parties">Regular disclosure of personal data to third
          parties</h2>
        <p>Personal data is not regularly disclosed to third parties.</p>
        <h2 id="data-retention">Data retention</h2>
        <p>The following data is stored in a local database, for the duration of a campaign and its publication
          period:</p>
        <pre><code>
          - Name
          - Email Address
        </code></pre>
        <p>The user may ask to be removed from the service by interacting with the contact person for the service.
          Access logs are deleted after 12 months.</p>
        <h2 id="transfer-of-personal-data-outside-the-eu-or-eea">Transfer of personal data outside the EU or EEA</h2>
        <p>Personal data shall not be transferred to any third party outside the EU.</p>
        <h2 id="how-to-access-rectify-and-delete-the-personal-data">How to access, rectify and delete the personal
          data</h2>
        <p>Contact for the service is provided. To rectify the data released by a Home Organisation, contact the Home
          Organisation’s operators.</p>
        <h2 id="data-protection-code-of-conduct">Data protection code of conduct</h2>
        <p>Personal data will be protected according to the <a
          href="https://www.geant.org/uri/Pages/dataprotection-code-of-conduct.aspx">Code of Conduct for Service
          Providers</a>, a common standard for the research and higher education sector to protect the user’s privacy.
        </p>
        <h2 id="contact-information">Contact Information</h2>
        <p>KIT/SCC</p>
        <p>Herrmann-von-Helmholtz-Platz 1</p>
        <p>76344 Eggenstein-Leopoldshafen</p>
        <p>Tel + 49 721 6082 4659</p>
        <p>m-ops@lists.kit.edu</p>
        <h2 id="kit-helpdesk">KIT Helpdesk:</h2>
        <p><a href="http://www.scc.kit.edu/servicedesk/index.php">http://www.scc.kit.edu/servicedesk/index.php</a></p>
      </article>

    );
  }
}

export default withRouter(PrivacyComponent);
