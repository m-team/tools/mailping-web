import React from "react";
import { withRouter } from "react-router-dom";

class ServicesTableItemComponent extends React.Component {

    renderContacts = (service_id, contact_details) => {
        return contact_details.map((contact, index) => {
            return (
                <tr key={`${contact.contact_email}${ contact.responded_at }${index}`}>
                    <th scope="row">{ index === 0 ? service_id : '' }</th>
                    <td>{ contact.contact_email }</td>
                    <td>{ contact.response_duration }</td>
                    <td>{ contact.responded_at }</td>
                    <td>{ contact.response_type }</td>
                    <td>{ contact.reply_address }</td>
                </tr>
            )
        })
    };

    render() {
        const { service_id, contact_details } = this.props;
        return ( contact_details && this.renderContacts( service_id, contact_details))
    }
}

export default withRouter(ServicesTableItemComponent);
