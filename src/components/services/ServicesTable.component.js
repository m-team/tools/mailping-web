import React from 'react';
import ServicesTableItemComponent from './ServicesTableItem.component'

const CampaignsTableHeaderComponent = () => {
    return (
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Security Contact Email</th>
            <th scope="col">Response Time</th>
            <th scope="col">Responded At</th>
            <th scope="col">Response Type</th>
            <th scope="col">Replied From</th>
        </tr>
        </thead>
    )
};

class ServicesTableComponent extends React.Component {

    render() {
        const { services } = this.props;

        return services && (
            <table className="table mb-0">
                <CampaignsTableHeaderComponent/>
                <tbody>
                    { services.map((service) => (<ServicesTableItemComponent key={service.service_id} { ...service }/>)) }
                </tbody>
            </table>
        );
    }
}

export default ServicesTableComponent;
