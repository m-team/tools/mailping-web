import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import ServicesTableComponent from "./ServicesTable.component";
import NotFoundPanelComponent from "../common/NotFoundPanel.component";
import { MailingApiClient } from "../../apiClients/mailpingApiClient";
import SpinnerComponent from "../common/Spinner.component";

class ServicesComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            campaign_name: null,
            campaign_id: null,
            services: null,
            error: null,
            isLoading: true
        }
    }

    componentWillMount() {
        const { match: { params: { campaign_id } }} = this.props;
        MailingApiClient.getCampaignServicesById(campaign_id)
            .then((response) => {
                const { campaign_name, campaign_id, services } = response.data;
                this.setState({ campaign_name: campaign_name, services: services, campaign_id: campaign_id, isLoading: false });
            })
            .catch((error) => {
                this.setState({ error: error, isLoading: false });
            });
    }


    getCampaignDetailsNavLabel = () => {
        const { campaign_name } = this.state;
        return campaign_name
            ? `${campaign_name} Details`
            : `Campaign Details`
    };

    render() {
        const { campaign_id, services, isLoading } = this.state;

        return isLoading
          ? ( <SpinnerComponent/> )
          : (
            <div className="h-100 p-3 w-100 d-flex flex-column overflow-hidden">
              <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                  <li className="breadcrumb-item"><Link to={`/campaigns/all`}>Campaigns</Link></li>
                  <li className="breadcrumb-item"><Link to={`/campaigns/${campaign_id}`}>{this.getCampaignDetailsNavLabel()}</Link></li>
                  <li className="breadcrumb-item active" aria-current="page">Services</li>
                </ol>
              </nav>

              <div className="card d-flex flex-column h-100 overflow-hidden">
                <div className="card-body h-100 overflow-hidden d-flex flex-column">
                  <h4 className="card-title">Services</h4>
                  <div className="h-100 overflow-auto">
                    { services && services.length
                      ? <ServicesTableComponent services={ services }/>
                      : <NotFoundPanelComponent error={"There are no services yet"}/>}
                  </div>
                </div>
              </div>
            </div>
        )
    }
}

export default withRouter(ServicesComponent);
