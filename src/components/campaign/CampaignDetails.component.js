import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import PingsDoughnutChartComponent from "../../charts/PingsDoughnutChart.component";
import { Icons } from "../../icons";
import CampaignDetailsCardBodyComponent from "./CampaignDetailsCardBody.component";
import toastr from 'toastr'
import RepingConfirmationModalComponent from "./RepingConfirmationModal.component";
import RepingDetailsCardBodyComponent from "./RepingDetailsCardBody.component";
import PingsLineChartComponent from "../../charts/PingsLineChart.component";
import { MailingApiClient } from "../../apiClients/mailpingApiClient";

class CampaignDetailsComponent extends React.Component {
    handleReping = () => {
        const { campaign: { campaign_id } } = this.props;

        MailingApiClient.repingCampaignById(campaign_id)
          .then((response) => {
              const { data: { payload: { repings, reping_services }}} = response;
              toastr.success(`${ repings } contacts have been successful repinged. Repinged services: ${ reping_services.join(',') }`);
            })
          .catch((error) => {
              toastr.error('Could not reping contacts. Please consider using the reping command on mailping host');
          });
    };

    getPingsDetails = (campaign) => {
        return campaign && (({ responded_pings_reply_count, responded_pings_link_count, not_responded_pings_count }) => (
          { responded_pings_reply_count, responded_pings_link_count, not_responded_pings_count }))(campaign)
    };

    renderRepingConfirmationModal = () => {
      const { campaign: { not_responded_pings_count } } = this.props;
      return (
        <div
          className="modal fade"
          id="repingConfirmationModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="repingConfirmationModal"
          aria-hidden="true">
          <RepingConfirmationModalComponent
            notRespondedPingsCount={ not_responded_pings_count }
            onReping={ this.handleReping }
          />
        </div>
      )
    };

    render() {
        const {  campaign, match: { url } } = this.props;

        return campaign && (
            <div className="d-flex flex-column h-100 overflow-auto">
                { this.renderRepingConfirmationModal() }

                <div className="d-flex flex-column flex-xl-row">
                    <div className="flex-1 card">
                        <div className="card-body">
                            <div className="card-title d-flex flex-row justify-content-between m-0">
                                <h4 className="m-0">{ campaign.campaign_name }</h4>
                                <h4 className="m-0"><span className="badge badge-info">ID: { campaign.campaign_id ? campaign.campaign_id : 'N/A' }</span> </h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="d-flex flex-column flex-xl-row">
                    <div className="card flex-1 card-details-panel mt-3">
                        <CampaignDetailsCardBodyComponent
                          colorClass={"badge-info"}
                          title={"Created"}
                          icon={Icons.CalendarCheckIcon}
                          data={ campaign.created_at ? campaign.created_at : 'N/A' }
                        />
                    </div>

                    <div className="card card-details-panel flex-1 mt-3 ml-xl-3">
                        <RepingDetailsCardBodyComponent
                          notRespondedPingsCount={ campaign.not_responded_pings_count }
                        />
                    </div>

                    <div className="card flex-1 card-details-panel mt-3 ml-xl-3">
                        <CampaignDetailsCardBodyComponent
                          colorClass={"badge-info"}
                          title={"First Response"}
                          icon={Icons.ReplyAllIcon}
                          data={ campaign.first_response_at ? campaign.first_response_at : 'N/A' }
                        />
                    </div>
                </div>

                <div className="d-flex flex-column flex-xl-row">
                    <div className="card flex-1 mt-3 card-details-panel">
                        <CampaignDetailsCardBodyComponent
                          colorClass={"badge-light"}
                          title={"Signing Method"}
                          icon={Icons.LockIcon}
                          data={ campaign.sign_method ? campaign.sign_method : 'N/A' }
                        />
                    </div>

                    <div className="card flex-1 card-details-panel mt-3 ml-xl-3">
                        <CampaignDetailsCardBodyComponent
                          colorClass={"badge-info"}
                          title={"Average Response Time"}
                          icon={Icons.ClockHistoryIcon}
                          data={ campaign.average_response_duration ? campaign.average_response_duration : 'N/A' }
                        />
                    </div>

                    <div className="card card-details-panel flex-1 mt-3 ml-xl-3">
                        <div className={`card-body d-flex flex-column justify-content-between badge-light`}>
                            <div className="card-title d-flex flex-row justify-content-between">
                                <h5>Covered Services</h5>
                                <div>
                                    <Link to={`${url}/services`} >
                                        <button className="btn btn-info">
                                            Show more
                                        </button>
                                    </Link>
                                </div>
                            </div>
                            <div className="d-flex flex-row justify-content-between align-items-baseline">
                                <div>{ Icons.ToolsIcon }</div>
                                <h3 className="mb-0">{ campaign.services_count ? campaign.services_count : 'N/A' }</h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="d-flex flex-column flex-xl-row">
                    <div className="card mt-3 flex-1">
                        <div className="card-body">
                            <div className="card-title d-flex flex-row justify-content-between">
                                <h4>Pings</h4>
                                <h4><span className="badge badge-info">Total: { campaign.pings_count ? campaign.pings_count : 'N/A' }</span> </h4>
                            </div>

                            <PingsDoughnutChartComponent pingsDetails={ this.getPingsDetails(campaign) }/>
                        </div>
                    </div>
                    <div className="card mt-3  ml-xl-3 ml-0 flex-1">
                        <div className="card-body">
                            <div className="card-title d-flex flex-row justify-content-between">
                                <h4>Response Timeline</h4>
                            </div>

                            <PingsLineChartComponent/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default withRouter(CampaignDetailsComponent)
