import React from 'react';

class RepingConfirmationModalComponent extends React.Component {

  render() {
    const { notRespondedPingsCount, onReping } =this.props;

    return (
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">Reping { notRespondedPingsCount } contacts</h5>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            Due to security reasons, the reping mails will not be signed if you initiate the reping process using the website since you need to enter the passphrase for your keys.
            <br/>
            If you want to send signed repings please use reping command on the mailping host.
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal">Cancel</button>
            <button
              type="button"
              className="btn btn-info"
              data-dismiss="modal"
              onClick={ onReping }>Reping without signing</button>
          </div>
        </div>
      </div>
    );
  }
}

export default RepingConfirmationModalComponent;
