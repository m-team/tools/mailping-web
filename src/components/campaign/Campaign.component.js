import React from 'react';
import { MailingApiClient } from "../../apiClients/mailpingApiClient";
import { Link, withRouter } from 'react-router-dom';
import ErrorPanelComponent from "../common/ErrorPanel.component";
import CampaignDetailsComponent from './CampaignDetails.component'
import NotFoundPanelComponent from "../common/NotFoundPanel.component";
import SpinnerComponent from "../common/Spinner.component";

const NOT_FOUND = 404;
const BAD_REQUEST = 500;

class CampaignComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            campaign: null,
            error: null,
            errorStatusCode: null,
            isLoading: true
        }
    }

    componentWillMount() {
        const { match: { params: { campaign_id } }} = this.props;

        MailingApiClient.getCampaignById(campaign_id)
            .then((response) => {
                this.setState({ campaign: response.data, isLoading: false })
            })
            .catch((error) => {
                let errorStatusCode = null;
                if (error.toString().match(/404/g)) {
                    errorStatusCode = 404
                } else if (error.toString().match(/500/g)) {
                    errorStatusCode = 500
                }

                this.setState({ error: error, isLoading: false, errorStatusCode });
            });
    }

    getCampaignDetailsNavLabel = () => {
        const { campaign } = this.state;
        return campaign && campaign.campaign_name
            ? `${campaign.campaign_name} Details`
            : `Campaign Details`
    };

    renderCampaign = () => {
        const { campaign, errorStatusCode } = this.state;
        const { match: { params: { campaign_id } }} = this.props;

        let CampaignComponent, error = null;

        switch (errorStatusCode) {
            case BAD_REQUEST:
                CampaignComponent = ErrorPanelComponent;
                break;
            case NOT_FOUND:
                CampaignComponent = NotFoundPanelComponent;
                error = `Campaign with id [${campaign_id}] not found`;
                break;
            default:
                CampaignComponent = CampaignDetailsComponent;
                break;
        }

        return <CampaignComponent campaign={ campaign } error={ error }/>
    };

    render() {
        const { isLoading } = this.state;

        return isLoading
            ? ( <SpinnerComponent/> )
            : (
            <div className="h-100 w-100 p-3 d-flex flex-column overflow-hidden">
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to={`/campaigns/all`}>Campaigns</Link></li>
                        <li className="breadcrumb-item active" aria-current="page">{this.getCampaignDetailsNavLabel()}</li>
                    </ol>
                </nav>
                { this.renderCampaign() }
            </div>
        );
    }
}

export default withRouter(CampaignComponent);
