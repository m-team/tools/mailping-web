import React from 'react';

class RepingDetailsCardBodyComponent extends React.Component {

  renderRepingCard = (notRespondedPingsCount) => {
    return notRespondedPingsCount > 0
      ? (<div className="h-100 d-flex flex-column align-items-center justify-content-center">
        <h5>{ notRespondedPingsCount } contacts did not respond</h5>
        <button className="btn btn-info" type="button" data-toggle="modal" data-target="#repingConfirmationModal">
          Reping { notRespondedPingsCount } contacts
        </button>
      </div>)
      : (<div className="h-100 d-flex flex-column align-items-center justify-content-center">
        <h5>All contacts responded</h5>
      </div>)
  };

  render() {
    const { notRespondedPingsCount } =this.props;

    return (
      <div className={`card-body d-flex flex-column justify-content-between badge-light justify-content-center`}>
        { this.renderRepingCard(notRespondedPingsCount)}
      </div>
    );
  }
}

export default RepingDetailsCardBodyComponent;
