import React from 'react';

class CampaignDetailsCardBodyComponent extends React.Component {

    render() {
        const { title, icon, data, colorClass } =this.props;

        return (
            <div className={`card-body d-flex flex-column justify-content-between ${colorClass}`}>
                <h5 className="card-title">{title}</h5>
                <div className="d-flex flex-row justify-content-between align-items-baseline">
                    <div>{ icon }</div>
                    <h4 className="mb-0">{ data ? data : 'N/A' }</h4>
                </div>
            </div>
        );
    }
}

export default CampaignDetailsCardBodyComponent;
