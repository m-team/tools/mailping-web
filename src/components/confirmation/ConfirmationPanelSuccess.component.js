import React from 'react';
import { Icons } from "../../icons";

const getServiceIdsAsString = (service_ids) => {
    return service_ids.join(', ');
};

const ConfirmationPanelSuccessComponent = ({response_duration, service_ids}) => {

    return (
        <div className="card-text d-flex flex-direction-row justify-content-center align-items-center">
            { Icons.CheckCircleIcon }
            <div className="pl-3">
                Thank you for confirmation. Your response time {response_duration} for service(s) ID { getServiceIdsAsString(service_ids)} has been successfully saved.
            </div>
        </div>
    );
};

export default ConfirmationPanelSuccessComponent;
