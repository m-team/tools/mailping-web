import React from 'react';
import { Icons } from "../../icons";

const REPLY_METHODS = {
    EMAIL_REPLAY: 'email reply',
    LINK: 'link',
};

const getIconByReplyMethod = (replyMethod) => {
    if (replyMethod === REPLY_METHODS.LINK) {
        return (Icons.LinkIcon)
    }
    if (replyMethod === REPLY_METHODS.EMAIL_REPLAY) {
        return (Icons.EnvelopeOpenIcon)
    }
};

const ConfirmationPanelNotModifiedComponent = ({reply_method, responded_at}) => {
    return (
        <div className="card-text d-flex flex-direction-row justify-content-center align-items-center">
            { getIconByReplyMethod(reply_method) }
            <div className="pl-3">
                You have already replied via { reply_method } on { responded_at }. This reply will not be saved.
            </div>
        </div>
    );
};

export default ConfirmationPanelNotModifiedComponent;
