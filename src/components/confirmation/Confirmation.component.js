import React from 'react';
import { withRouter } from 'react-router-dom';
import { MailingApiClient } from "../../apiClients/mailpingApiClient";

import ConfirmationPanelBadRequestComponent from "./ConfirmationPanelBadRequest.component";
import ConfirmationPanelNotModifiedComponent from "./ConfirmationPanelNotModified.component";
import ConfirmationPanelSuccessComponent from "./ConfirmationPanelSuccess.component";
import ErrorPanelComponent from "../common/ErrorPanel.component";
import toastr from "toastr";

const CONFIRMATION_STATUSES = {
    SUCCESS: 'success',
    NOT_MODIFIED: 'not_modified',
    BAD_REQUEST: 'bad_request'
};

class ConfirmationComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            confirmation: null,
            error: null,
        }
    }

    componentWillMount() {
        const code = new URLSearchParams(this.props.location.search).get('code');

        MailingApiClient.getConfirmation(code)
            .then((response) => {
                this.setState({ confirmation: response.data })
            })
            .catch((error) => {
                toastr.error('Ups! Something went wrong...');
                this.setState({ error: error });
            });
    }

    renderConfirmationPanelByStatus = () => {
        const { confirmation: { status, payload } } = this.state;

        let PanelComponent = null;
        switch (status) {
            case CONFIRMATION_STATUSES.SUCCESS:
                PanelComponent = ConfirmationPanelSuccessComponent;
                break;
            case CONFIRMATION_STATUSES.NOT_MODIFIED:
                PanelComponent = ConfirmationPanelNotModifiedComponent;
                break;
            case CONFIRMATION_STATUSES.BAD_REQUEST:
                PanelComponent = ConfirmationPanelBadRequestComponent;
                break;
            default:
                break;
        }

        return <PanelComponent { ...payload }/>
    };

    renderCard = () => {
        return (
            <div className="card">
                <div className="card-body">
                    { this.renderConfirmationPanelByStatus() }
                </div>
            </div>
        )
    };

    render() {
        return (
            <div className="h-100 w-100 d-flex justify-content-center align-items-center">
                { this.state.confirmation && this.renderCard() }
                { this.state.error && <ErrorPanelComponent/> }
            </div>
        );
    }
}

export default withRouter(ConfirmationComponent);
