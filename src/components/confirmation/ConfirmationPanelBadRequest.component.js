import React from 'react';
import { Icons } from "../../icons";

const ConfirmationPanelBadRequestComponent = () => {

    return (
        <div className="card-text d-flex flex-direction-row justify-content-center align-items-center">
            { Icons.ExclamationCircleIcon }
            <div className="pl-3">
                Unfortunately, we could not find the provided confirmation link. <br/>
                We would kindly ask you to check the confirmation link or alternatively reply to our email.
            </div>
        </div>
    );
};

export default ConfirmationPanelBadRequestComponent;
