import React from 'react';
import CampaignsTableComponent from "./CampaignsTable.component";
import { MailingApiClient } from "../../apiClients/mailpingApiClient";
import NotFoundPanelComponent from "../common/NotFoundPanel.component";
import SpinnerComponent from "../common/Spinner.component";

class CampaignsComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            campaigns: null,
            error: null,
            isLoading: true
        }
    }

    componentWillMount() {
        MailingApiClient.getCampaigns()
            .then((response) => {
                this.setState({ campaigns: response.data, isLoading: false })
            })
            .catch((error) => {
                this.setState({ error: error, isLoading: false });
            });
    }

    render() {
        let { campaigns, isLoading } = this.state;

        return isLoading
            ? ( <SpinnerComponent/> )
            : (
                <div className="h-100 p-3 w-100 d-flex flex-column overflow-hidden">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item active" aria-current="page">Campaigns</li>
                        </ol>
                    </nav>

                    <div className="card d-flex flex-column h-100 overflow-hidden">
                        <div className="card-body h-100 overflow-hidden d-flex flex-column">
                            <h4 className="card-title">Campaigns</h4>
                            <div className="h-100 overflow-auto">
                                {
                                    campaigns && campaigns.length > 0
                                        ? <CampaignsTableComponent campaigns={ campaigns }/>
                                        : <NotFoundPanelComponent error={"There are no campaigns yet"}/>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            )
    }
}

export default CampaignsComponent;
