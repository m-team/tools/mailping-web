import React from 'react';
import CampaignsTableItemComponent from './CampaignsTableItem.component'

const CampaignsTableHeaderComponent = () => {
    return (
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Created At</th>
                <th scope="col">Signing Method</th>
            </tr>
        </thead>
    )
};

class CampaignsTableComponent extends React.Component {

    render() {
        const { campaigns } = this.props;

        return campaigns && (
            <table className="table mb-0">
                <CampaignsTableHeaderComponent/>
                <tbody>
                { campaigns.map((campaign) => (
                    <CampaignsTableItemComponent key={campaign.campaign_id} { ...campaign }/>)
                ) }
                </tbody>
            </table>
        );
    }
}

export default CampaignsTableComponent;
