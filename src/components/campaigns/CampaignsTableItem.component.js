import React from "react";
import { withRouter, Link } from "react-router-dom";

class CampaignsTableItemComponent extends React.Component {
    render() {
        const { campaign_id, campaign_name, created_at, sign_method } = this.props;

        return (
            <tr>
                <th scope="row">{ campaign_id }</th>
                <td> <Link to={`${campaign_id}`}>{ campaign_name }</Link> </td>
                <td>{ created_at }</td>
                <td>{ sign_method }</td>
            </tr>
        )
    }
}

export default withRouter(CampaignsTableItemComponent);
