import React from 'react';
import { Icons } from "../../icons";

const NotFoundPanelComponent = ({error}) => {

    return (
        <div className="d-flex flex-direction-row justify-content-center align-items-center" role="alert">
            { Icons.SearchIcon }
            <div className="pl-3">
                <h4 className="mb-0">{ error }</h4>
            </div>
        </div>
    );
};

export default NotFoundPanelComponent;
