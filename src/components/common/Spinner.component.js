import React from 'react';

const SpinnerComponent = () => {

    return (
        <div className="h-100 w-100 d-flex justify-content-center align-items-center loading-label">
            <h1>Loading...</h1>
        </div>
    );
};

export default SpinnerComponent;
