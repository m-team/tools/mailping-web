import React from 'react';
import { Icons } from "../../icons";

const ErrorPanelComponent = ({error}) => {

    return (
        <div className="d-flex flex-direction-row justify-content-center align-items-center" role="alert">
            { Icons.EmojiFrownIcon }
            <div className="pl-3">
                <h4 className="mb-0">Doh! Something went wrong...</h4>
            </div>
        </div>
    );
};

export default ErrorPanelComponent;
