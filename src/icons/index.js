import { LinkIcon } from "./Link.icon";
import { ExclamationCircleIcon } from "./ExclamationCircle.icon";
import { EnvelopeOpenIcon } from "./EnvelopeOpen.icon";
import { CheckCircleIcon } from "./CheckCircle.icon";
import { EmojiFrownIcon } from "./EmojiFrown.icon";
import { SearchIcon } from "./Search.icon";
import { ReplyAllIcon } from "./ReplyAll.icon";
import { ClockHistoryIcon } from "./ClockHistory.icon";
import { LockIcon } from "./Lockicon";
import { CalendarCheckIcon } from "./CalendarCheck.icon";
import { ToolsIcon } from "./Tools.icon";

export const Icons = {
    LinkIcon,
    ExclamationCircleIcon,
    EnvelopeOpenIcon,
    CheckCircleIcon,
    EmojiFrownIcon,
    SearchIcon,
    ReplyAllIcon,
    ClockHistoryIcon,
    LockIcon,
    CalendarCheckIcon,
    ToolsIcon
};
