import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import CampaignsComponent from "./components/campaigns/Campaigns.component";
import ConfirmationComponent from "./components/confirmation/Confirmation.component";
import CampaignComponent from "./components/campaign/Campaign.component";
import ServicesComponent from "./components/services/Services.component";

import 'toastr/build/toastr.css'
import './App.css';
import PrivacyComponent from "./components/privacy/Privacy.component";

function App() {
  return (
    <div className="h-100 app-container">
      <Router>
        <Switch>
          <Route path="/privacy">
            <PrivacyComponent/>
          </Route>
          <Route path="/confirmation">
            <ConfirmationComponent />
          </Route>
          <Route path="/campaigns/all">
            <CampaignsComponent/>
          </Route>
          <Route path="/campaigns/:campaign_id/services">
            <ServicesComponent/>
          </Route>
          <Route path="/campaigns/:campaign_id">
            <CampaignComponent/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
