import React from 'react';
import { Doughnut } from 'react-chartjs-2';

const LABEL_BY_KEY = {
    responded_pings_reply_count: 'Response via email reply',
    responded_pings_link_count: 'Response via link',
    not_responded_pings_count: 'Without response',
};

class PingsDoughnutChartComponent extends React.Component {

    processChartData = () => {
        const { pingsDetails } = this.props;
        const labels = [];
        const data = [];
        const pingsDetailsTransformed = Object.entries(pingsDetails);

        pingsDetailsTransformed.forEach(([key, value]) => {
            // labels.push(`${LABEL_BY_KEY[key]} (${value})`);
            labels.push(`${LABEL_BY_KEY[key]}`);
            data.push(value)
        });

        return {
            labels,
            data
        }
    };

    render() {
        const chartData = this.processChartData();
        const data = {
            labels: chartData.labels,
            datasets: [
                {
                    backgroundColor: [
                        'rgba(54, 162, 235, 0.5)',
                        'rgba(255, 206, 86, 0.5)',
                        'rgba(255, 99, 132, 0.5)'
                    ],
                    borderColor: [
                        'rgba(54, 162, 235, 0.5)',
                        'rgba(255, 206, 86, 0.5)',
                        'rgba(255, 99, 132, 0.5)'
                    ],
                    hoverBackgroundColor: [
                        'rgba(54, 162, 235, 0.7)',
                        'rgba(255, 206, 86, 0.7)',
                        'rgba(255, 99, 132, 0.7)'
                    ],
                    data: chartData.data
                }
            ]
        };

        return (
            <Doughnut
                data={data}
                options={{
                    legend:{
                        display:true,
                        position:'bottom'
                    }
                }}
            />
        )
    }

}

export default PingsDoughnutChartComponent
