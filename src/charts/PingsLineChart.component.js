import React from 'react';
import { Scatter } from 'react-chartjs-2';
import { MailingApiClient } from "../apiClients/mailpingApiClient";
import { withRouter } from 'react-router-dom';

class PingsLineChartComponent extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            chartData: null,
            error: null,
            isLoading: true
        }
    }

    componentWillMount() {
        const { match: { params: { campaign_id } }} = this.props;

        MailingApiClient.getCampaignTimelineId(campaign_id)
          .then((response) => {
              this.setState({ chartData: response?.data?.payload, isLoading: false })
          })
          .catch((error) => {
            // toastr.error('Ups! Something went wrong...');
            this.setState({ error: error, isLoading: false });
          });

    }

    renderChart = () => {
      const { chartData } = this.state;
      const data = {
        datasets: [{
          data: chartData,
          pointRadius: 7,
          pointHoverRadius: 7,
          borderColor: 'rgba(33, 131, 146, 0.5)',
          hoverBackgroundColor: 'rgba(23, 162, 184, 0.8)',
          showLine: false,
          fill:false
        }]
      };

      return chartData ? (
        <Scatter
          data={data}
          options={{
            legend:{
              display:false
            },
            tooltips: {
              callbacks: {
                title: function(tooltipItem, data) {
                  const index = tooltipItem[0]['index'];
                  const dataset = data.datasets[0].data[index]
                  const text = `Contact Email: ${ dataset.contact_email }\nServices: ${ dataset.services }\nResponse Time: ${ dataset.response_duration }\nResponded At: ${ dataset.responded_at }\nResponse Type: ${ dataset.response_type }`
                  return text
                },
                label: function(tooltipItem, data) {},
                afterLabel: function(tooltipItem, data) {}
              }
            },
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                },
                scaleLabel: {
                  display: true,
                  labelString: 'Services Responded (accumulated)'
                }
              }],
              xAxes: [{
                ticks: {
                  type: 'time',
                  beginAtZero: true
                },
                scaleLabel: {
                  display: true,
                  labelString: 'Time (s)'
                }
              }]
            }
          }}
        />
      ) : (<div className="d-flex w-100 h-100 align-items-center justify-content-center"><div>No data</div></div>)
    };

    render() {
        const { isLoading } = this.state;

        return !isLoading && this.renderChart()
    }
}

export default withRouter(PingsLineChartComponent)
