const CONNECTION_TIMEOUT = 30 * 1000

export const createConfiguration = (apiUrl) => {
    return {
        baseURL: apiUrl,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        timeout: CONNECTION_TIMEOUT,
    }
};
