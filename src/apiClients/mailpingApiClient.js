import axios from 'axios'

import { createConfiguration } from './axios.config'

const SERVICE_URL = 'https://mailping.data.kit.edu/'


class ApiClient {
    constructor(axiosClient) {
        this.client = axiosClient
    }

    getConfirmation(code) {
        return this.client.get(`public-api/confirmation/${code}`)
    }

    getCampaigns() {
        return this.client.get(`api/campaigns`)
    }

    getCampaignById(campaign_id) {
        return this.client.get(`api/campaigns/${campaign_id}`)
    }

    getCampaignServicesById(campaign_id) {
        return this.client.get(`api/campaigns/${campaign_id}/services`)
    }

    repingCampaignById(campaign_id) {
        return this.client.get(`api/campaigns/${campaign_id}/reping`)
    }

    getCampaignTimelineId(campaign_id) {
        return this.client.get(`api/campaigns/${campaign_id}/timeline`)
    }
}

export const MailingApiClient = new ApiClient(axios.create(createConfiguration(SERVICE_URL)));
